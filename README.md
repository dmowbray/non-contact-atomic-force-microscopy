# Non-Contact Atomic Force Microscopy
Non-Contact Atomic Force Microscopy Simulator
for GPAW and DFTD3

This module defines an NonContactAFM class
for performing non contact AFM (nc-AFM) simulations
employing the method described in

[Sader, J. E. and Jarvis, S. P., *Appl. Phys. Lett.* 2004, **84**, 1801--1803.](http://dx.doi.org/10.1063/1.1667267 "doi:10.1063/1.1667267")

based on the effective potential calculated with GPAW

[Enkovaara, J. et al, *J. Phys.: Cond. Matt.* 2010, **22**, 253002.](http://dx.doi.org/10.1088/0953-8984/22/25/253202 "doi:10.1088/0953-8984/22/25/253202")

and/or the force on a CO probe calculated with DFTD3

[Grimes, S.; Antony, J.;  Ehrlich, S.; and Krieg, H., *J. Chem. Phys.*, 2010, **132**, 154104.](http://dx.doi.org/10.1063/1.3382344 "doi:10.1063/1.3382344")

Installation:

$ gpaw-python setup.py install

The ncafm.py script may be either executed directly
from the command line or loaded as a Python module.
For help on the command line interface try:

$ non-contact-afm --help

