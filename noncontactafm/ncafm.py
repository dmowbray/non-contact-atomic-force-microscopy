#!/usr/bin/env gpaw-python
# -*- coding: utf-8 - *-

"""This module performs a
non-contact atomic force microscopy (nc-AFM)
simulation using either the effective potential veff
or the electrostatic potential Velec as obtained
from a GPAW calculator, or the dispersion forces
obtained from DFTD3.  The tip descends from the z-axis"""

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

from os.path import exists
from pickle import dump, load
from argparse import ArgumentParser
from math import pi
from mpi4py import MPI
from matplotlib.pyplot import gca, contourf, plot, xlim, ylim, savefig
from matplotlib.pyplot import colorbar, show, cm
from numpy import indices, dot, linspace, arange, zeros
from numpy.linalg import norm
from numpy.lib.shape_base import tile
from ase import Atom, Atoms
from ase.io import read
from ase.io.cube import read_cube_data
from ase.parallel import parprint
from ase.calculators.dftd3 import DFTD3
from gpaw import GPAW



class NonContactAFMInterface(object):
    """Interface for performing non contact AFM (nc-AFM) simulations
    employing the method described in
    J. E. Sader and S. P. Jarvis,
    [Appl. Phys. Lett., 84, (2004), 1801--1803]."""

    default_parameters = {
        'verbose': False,
        'zmin': 1.0,
        'amplitude': 0.6, # [Å]
        'wres': 20878.6, # [Hz]
        'k': 112.35,
        'h': 0.1,
        'repeatx': 1,
        'repeaty': 1,
        'gridrefinement': 2,
        'redo': False,
        'plotwireframe': True,
        'method': '',
        'CObond': 1.1492275910372003}

    def __init__(self, filename, **kwargs):
        """Creates a NonContactAFM object.

        filename	input filename
        zmin		Minimum height above the atoms in [Å]
        amplitude	Amplitude of oscillation in [Å]
        wres		Resonant frequency in [Hz]
        k     	        Spring constant of the probe in [eV/Å²]
        h		Grid spacing in [Å]
        repeatx	        Repetition of cell in x-direction
        repeaty	        Repetition of cell in y-direction
        gridrefinement	Refinement of grid for effective potential
        redo		Ignore pickle files (boolean)
        verbose		Verbose output (boolean)
        plotwireframe	Plot wire frame molecular structure (boolean)
        method		Calculation method"""
        self.kwargs = kwargs
        self.set_default_arguments()
        self.filename = filename
        self.deltaw = None	        # [Nx, Ny] array of frequency changed
        self.veff = None	        # [Nx, Ny, Nz] array of effective potential
        self.forces = None	        # [Nx, Ny, Nz] array of effective potential
        self.shape = None		# size of effective potential and forces arrays
        self.atoms = Atoms()       # ASE list of atoms
        self.load(filename)

    def set_default_arguments(self):
        """Specify default keyword arguments from
        default_parameters

        wres		20878.6 Resonant frequency in [Hz]
        k     	                112.35 Spring constant of the probe in [eV/Å²]
        repeatx	        1 Repetition of cell for plotting
        repeaty	        1 Repetition of cell for plotting
        gridrefinement	1 Refinement of grid for effective potential
        redo		False Ignore pickle files (boolean)
        verbose		False Verbose output (boolean)
        plotwireframe	True Plot wire frame molecular structure (boolean)
        method		Calculation method"""
        if 'verbose' not in self.kwargs:
            self.kwargs['verbose'] = self.default_parameters['verbose']
        for key in self.default_parameters:
            if key not in self.kwargs:
                self.kwargs[key] = self.default_parameters[key]
            self.verboseprint(key, self.kwargs[key])

    def be_verbose(self, verbose=True):
        """Provide verbose messaging on rank 0"""
        self.kwargs['verbose'] = verbose
        return verbose

    def verboseprint(self, *args, **kwargs):
        """MPI-safe verbose print
        Prints only from master if verbose is True."""
        if self.kwargs['verbose']:
            parprint(*args, **kwargs)

    def load(self, loadfile):
        """Load simulation data from loadfile or read atoms

        Recognizes filenames ending with 'pckl' or 'gpw' and
        GPAW and pickle instances"""
        if isinstance(loadfile, str):
            ending = '.'+loadfile.split('.')[-1]
            self.verboseprint("Ending", ending)
            self.verboseprint("File name", loadfile)
            if exists(loadfile) and not self.kwargs['redo']:
                if ending == '.pckl':
                    picklefile = open(loadfile, 'rb')
                    self.load_pickle(picklefile)
                    del picklefile
                elif ending == '.gpw':
                    calc = GPAW(loadfile, txt=None,
                                parallel={'domain': 1},)
                    self.load_gpw(calc)
                    del calc
                elif ending == '.cube':
                    cubefile = open(loadfile, 'r')
                    self.load_cube(cubefile)
                    del cubefile
                else:
                    atoms = read(loadfile)
                    self.load_atoms(atoms)
            else:
                raise ValueError("Missing file "+loadfile)
        elif isinstance(loadfile, GPAW):
            self.load_gpw(loadfile)
        elif isinstance(loadfile, file):
            self.load_pickle(loadfile)
        elif isinstance(loadfile, Atoms):
            self.load_atoms(loadfile)
        else:
            raise NotImplementedError(type(loadfile))
        if self.kwargs['redo'] or self.deltaw is None:
            self.calculate()

    def load_pickle(self, picklefile):
        """Load data from picklefile

        deltaw		Frequencies in [Nx, Ny] array
        kwargs		Keyword arguments
        shape		Shape of the potential/forces
        veff		        Effective potential numpy [Nx, Ny, Nz] array
        forces		Effective potential numpy [Nx, Ny, Nz] array
        filename		File name for loading self.atoms"""
        self.verboseprint("Loading Pickle File")
        deltaw, kwargs, shape, veff, forces, filename = load(picklefile)
        if kwargs['zmin'] != self.kwargs['zmin']:
            self.verboseprint('Height change from',
                              self.kwargs['zmin'], 'to',
                              kwargs['zmin'], 'Å')
        if kwargs['amplitude'] != self.kwargs['amplitude']:
            self.verboseprint('Amplitude change from',
                              self.kwargs['amplitude'], 'to',
                              kwargs['amplitude'], 'Å')
        self.kwargs['zmin'] = kwargs['zmin']
        self.kwargs['amplitude'] = kwargs['amplitude']
        self.deltaw = deltaw
        self.shape = shape
        self.veff = veff
        self.forces = forces
        self.atoms = read(filename)

    def load_gpw(self, calc):
        """Load data from GPAW gpw file.

        veff		Effective Potential in eV
        atoms	ASE list of atoms"""
        self.atoms = calc.get_atoms()
        self.verboseprint("Loading effective potential from GPAW file")
        self.veff = calc.get_effective_potential(
            #gridrefinement=self.kwargs['gridrefinement']
        )
        self.shape = self.veff.shape
        self.verboseprint("Effective potential successfully loaded",
                          self.shape, self.kwargs['gridrefinement'])

    def load_cube(self, cubefile):
        """Load data from a Gaussian cube file.

        Reads atoms and veff from cube file"""
        self.veff, self.atoms = read_cube_data(cubefile)
        self.atoms.set_pbc(True)
        self.shape = self.veff.shape

    def load_atoms(self, atoms):
        """Load data from ASE Atoms file.

        shape	grid based on grid spacing h and
        		unit cell from atoms
        atoms	ASE list of atoms"""
        self.atoms = atoms
        self.shape = self.get_shape(atoms.get_cell())

    def get_shape(self, cell):
        """Extract shape based on cell and grid spacing

        cell		Numpy 3x3 array of unit cell parameters in [Å]

        shape	int [Nx, Ny, Nz] array"""
        shape = [0, 0, 0]
        gridspace = self.kwargs['h']
        for i in range(3):
            shape[i] = int(round(norm(cell[i])/gridspace))
            #shape[i] += mod(shape[i], 2)
        return (shape[0], shape[1], shape[2])

    def get_my_range(self):
        """Return range of self.shape[0]
        indicies to calculate over for
        MPI parallelization.

        indicies	range(nstart, nend)"""
        rank = MPI.COMM_WORLD.rank
        size = MPI.COMM_WORLD.size
        nstart = int(round(float(self.shape[0])/size*rank))
        nend = int(round(float(self.shape[0])/size*(rank+1)))
        return range(nstart, nend)

    def get_curvature(self, zindex, findex, deltaz):
        """Return curvature calculated from the force or the
        effective potential

        zindex		Index of minimum height
        findex		Index of final height
        deltaz		Grid spacing in z-direction

        curvature	Derivative of the force in z direction in [eV/Å²]"""
        raise NotImplementedError("Curvature calculator")

    def write_pickle(self, picklefilename):
        """Write data to a picklefile.

        deltaw		Frequencies in [Nx, Ny] array
        zmin		Minimum height above highest atom in [Å]
        amplitude	Oscillation amplitude in [Å]"""
        if MPI.COMM_WORLD.rank == 0:
            if not picklefilename.endswith('.pckl'):
                ending = picklefilename.split('.')[-1]
                picklefilename = picklefilename.strip(ending)+'pckl'
            dump([self.deltaw, self.kwargs, self.shape,
                  self.veff, self.forces, self.filename],
                 open(picklefilename, 'wb'))


    def get_xy(self, repeat=False):
        """Generate shape d array of x and y positions

        shape array [Nx, Ny] of xy grid shape

        Returns:
        x 	[Nx, Ny] array of x positions in Å
        y 	[Nx, Ny] array of y positions in Å"""
        shape = self.shape[:2]
        repeatshape = shape
        if repeat:
            repeatx = self.kwargs['repeatx']
            repeaty = self.kwargs['repeaty']
            repeatshape = (repeatx * shape[0],
                           repeaty * shape[1])
            self.verboseprint("Repeating", repeatshape)
        indexes = indices(repeatshape,
                          dtype=float).reshape((2, -1)).T
        cell = self.atoms.get_cell()
        xarray, yarray = dot(indexes / shape, cell[:2, :2]).T.reshape(
            (2, ) + repeatshape)
        return xarray, yarray

    def calculate(self):
        """Calculate deltaw using Jarvis Method
        J. E. Sader and S. P. Jarvis, J. Phys. Chem, 84(10), 1801--1803.

        deltaw = - wres/(pi a k) int_{-1}^{1} F(z + a(1+u)) u/sqrt(1-u^2) du"""
        if self.shape is None:
            self.shape = self.get_shape(self.atoms.get_cell())
            self.verboseprint("Generated Shape", self.shape)
        self.verboseprint("Calculating deltaw")
        shape = self.shape
        self.verboseprint("Shape", shape)
        cell = self.atoms.cell
        self.verboseprint("Cell", cell)
        zmin = self.kwargs['zmin'] + self.atoms.positions[:, 2].max()
        self.verboseprint("Minimum Height", zmin)
        zindex = int(zmin/cell[2, 2]*shape[2])
        deltaz = cell[2, 2]/shape[2]
        aindex = int(round(self.kwargs['amplitude'] / deltaz)) * 2
        findex = zindex + aindex
        self.kwargs['amplitude'] = aindex * deltaz * 0.5
        prefactor = -self.kwargs['wres'] / (self.kwargs['amplitude'] *
                                            pi * self.kwargs['k'])
        uarray = linspace(-1, 1, aindex)
        curvature = self.get_curvature(zindex, findex, deltaz)
        self.deltaw = prefactor * (curvature * (1 - uarray**2)**0.5).sum(axis=-1)
        self.verboseprint("deltaw Calculated")
        self.kwargs['redo'] = False

    def plot(self, outfilename, wmin=None, wmax=None, cmap=None):
        """Write a contour plot to outfilename of deltaw
        for the given range

        outfilename	Name of output file passed to savefig (str)
        wmin		Minimum frequency in [Hz] to plot
        wmax		Maximum frequency in [Hz] to plot
        cmap		Color map to employ"""
        if MPI.COMM_WORLD.rank != 0:
            return
        if self.deltaw is None:
            self.verboseprint('deltaw still undefined')
        if wmin is None:
            wmin = self.deltaw.min()
        if wmax is None:
            wmax = self.deltaw.max()
        self.verboseprint('deltaw min', self.deltaw.min(),
                          'max', self.deltaw.max(),
                          'wmin', wmin, 'wmax', wmax)
        wscale = linspace(wmin, wmax, 100)
        if cmap is None:
            cmap = cm.Greys_r
        xarray, yarray = self.get_xy(repeat=True)
        deltaw = tile(self.deltaw, (self.kwargs['repeatx'],
                                    self.kwargs['repeaty']))
        self.verboseprint('x shape', xarray.shape, 'y shape', yarray.shape,
                          'deltaw shape', deltaw.shape)
        gca(aspect='equal')
        contourf(xarray, yarray, deltaw, wscale, cmap=cmap, extend='both')
        self.plot_wire_frame()
        xlim(xarray[0, 0], xarray[-1, -1])
        ylim(yarray[0, 0], yarray[-1, -1])
        colorbar()
        savefig(outfilename, dpi=300)

    def plot_wire_frame(self, bondlength=2.0):
        """Plot a wire frame model of the molecule

        bondlength	Maximum distance for a bond in [Å]"""
        if not self.kwargs['plotwireframe']:
            return
        istart = 0
        atoms = self.atoms
        self.verboseprint("Plotting Molecular Structure")
        # Skip all metal atoms
        while istart < len(atoms) and atoms[istart].number > 18:
            istart += 1
        for i in range(istart+1, len(atoms)):
            if not (atoms[i].number == 1 or atoms[i].number > 18):
                for j in range(istart, i):
                    if not (atoms[j].number == 1 or atoms[j].number > 18):
                        if atoms.get_distance(i, j) < bondlength:
                            plot([atoms.positions[i, 0],
                                  atoms.positions[j, 0]],
                                 [atoms.positions[i, 1],
                                  atoms.positions[j, 1]], 'r-')

class NCAFMForceInterface(NonContactAFMInterface):
    """Interface for using forces on a probe to perform
    non contact AFM (nc-AFM) simulations"""

    def load_force(self):
        """Return force on the probe in self.atoms
        calculated with the attached calculator

        force	force on probe in [eV/Å]"""
        raise NotImplementedError

    def add_calculator(self, outfilename=None):
        """Add a calculator to self.atoms for
        calculating forces on a probe.

        outfilename	str Label for output files"""
        raise NotImplementedError

    def add_probe(self, position=None):
        """Add a probe at the given position

        position	Probe position in [Å]"""
        raise NotImplementedError

    def move_probe(self, position):
        """Move the probe to the given position

        position	Probe position vector in [Å]"""
        raise NotImplementedError

    def remove_probe(self):
        """Remove the probe from self.atoms"""
        raise NotImplementedError


    def calculate_forces(self, zindex, findex, deltaz):
        """Obtain forces on a probe atom using
        ASE calculator from self.add_calculator().
        Performs MPI parallelization over calculations.

        zindex		int Index of minimum height
        findex		int Index of final height

        forces		numpy [Nx, Ny, Nz] array of forces in [eV/Å]"""
        if self.forces is not None and not self.kwargs['redo']:
            return self.forces
        self.verboseprint("Calculating Forces")
        forces = zeros(self.shape)
        self.forces = forces.copy()
        xarray, yarray = self.get_xy()
        zarray = arange(0, self.shape[2])*deltaz
        self.add_calculator()	# Add calculator to self.atoms
        self.add_probe() 		# Add probe to self.atoms
        # Use MPI parallelization over shape[0] indicies
        for i in self.get_my_range():
            for j in range(self.shape[1]):
                for hindex in range(zindex, findex):
                    # Move probe to position
                    self.move_probe([xarray[i, j], yarray[i, j], zarray[hindex]])
                    forces[i, j, hindex] = self.load_force()
            print("Max dF = ",
                  (forces[i, :, zindex+1:findex+1] -
                   forces[i, :, zindex-1:findex-1]).max()*0.5,
                  " eV/Å on probe on rank", MPI.COMM_WORLD.rank)
        # Remove probe from self.atoms
        self.remove_probe()
        # Sum forces calculated on each rank and scatter to all ranks
        MPI.COMM_WORLD.Allreduce(sendbuf=forces, recvbuf=self.forces)
        return self.forces


class NCAFMGPAW(NonContactAFMInterface):
    """Class for performing nc-AFM simulations
    employing the method described in
    J. E. Sader and S. P. Jarvis,
    [Appl. Phys. Lett., 84, (2004), 1801--1803].
    and using the effective potential from GPAW
    J. Enkovaara et al,
    [J. Phys.: Cond. Matt., 22, (2010), 253002]."""
    def get_curvature(self, zindex, findex, deltaz):
        """Return curvature calculated from the effective potential
        Using the Central Difference Method

        zindex		Index of minimum height
        findex		Index of final height
        deltaz		Grid spacing in z-direction

        curvature	Derivative of the force in z direction in [eV/Å²]"""
        self.verboseprint("Calculating Curvature")
        veff = self.veff
        curvature = (veff[:, :, zindex-1:findex-1] -
                     2*veff[:, :, zindex:findex] +
                     veff[:, :, zindex+1:findex+1])/deltaz
        self.verboseprint("Curvature shape", curvature.shape)
        return curvature

class NCAFMGPAW5Pt(NonContactAFMInterface):
    """Class for performing nc-AFM simulations
    employing the method described in
    J. E. Sader and S. P. Jarvis,
    [Appl. Phys. Lett., 84, (2004), 1801--1803].
    and using the effective potential from GPAW
    J. Enkovaara et al,
    [J. Phys.: Cond. Matt., 22, (2010), 253002]."""
    def get_curvature(self, zindex, findex, deltaz):
        """Return curvature calculated from the effective potential
        Using the Five Point Second Derivative Method

        zindex		Index of minimum height
        findex		Index of final height
        deltaz		Grid spacing in z-direction

        curvature	Derivative of the force in z direction in [eV/Å²]"""
        self.verboseprint("Calculating Curvature")
        veff = self.veff
        curvature = (-veff[:, :, zindex-2:findex-2] +
                     16*veff[:, :, zindex-1:findex-1] -
                     30*veff[:, :, zindex:findex] +
                     16*veff[:, :, zindex+1:findex+1] -
                     veff[:, :, zindex+2:findex+2])/deltaz/12.
        self.verboseprint("Curvature shape", curvature.shape)
        return curvature

class NCAFMDFTD3(NCAFMForceInterface):
    """Class for performing nc-AFM simulations using the
    force on the O atom in CO as a probe as
    calculated with DFTD3
    S. Grimes, J. Antony, S. Ehrlich and H. Krieg,
    [J. Chem. Phys., 132, (2010), 154104.]"""

    def load_force(self):
        """Return force on the probe in self.atoms
        calculated with the attached calculator

        force	force on probe in [eV/Å]"""
        # Calculate force in eV/Å on O atom
        force = self.atoms.get_forces()[-2, 2]
        #self.verboseprint("Fz =", force, "eV/Å on O atom")
        return force

    def add_calculator(self, outfilename=None):
        """Add a DFTD3 calculator to self.atoms

        outfilename	str Label for output files"""
        rank = str(MPI.COMM_WORLD.rank)
        if outfilename is None:
            endswith = self.filename.split('.')[-1]
            outfilename = self.filename.strip(endswith)+'dftd3'
        outfilename = outfilename+'_'+rank
        if self.atoms.calc is None or not isinstance(self.atoms.calc, DFTD3):
            calc = DFTD3(label=outfilename, runall=True)
            calc.directory = '/home/dmowbray/'+rank
            self.atoms.set_calculator(calc)

    def add_probe(self, position=None):
        """Add a probe at the given position
        Probe is the O atoms of a vertically aligned CO
        molecule to position, with the CO bond legnth
        from kwargs['CObond']

        position	O position in [Å]"""
        if position is None:
            position = [0, 0, 0]
        self.atoms.append(Atom('O', position))
        position[2] += self.kwargs['CObond']
        self.atoms.append(Atom('C', position))

    def move_probe(self, position):
        """Move the probe to the given position
        Probe is the O atom of a vertically aligned CO
        molecule to position, with the CO bond legnth
        from kwargs['CObond']

        position	O position in [Å]"""
        self.atoms.positions[-2:] = position
        self.atoms.positions[-1, 2] += self.kwargs['CObond']

    def remove_probe(self):
        """Remove the probe from self.atoms
        Probe is the O atom of a vertically aligned CO"""
        del self.atoms[-2:]

    def get_curvature(self, zindex, findex, deltaz):
        """"Generate curvature from DFTD3 forces
        using a Central Difference Method

        zindex		Index of minimum height
        findex		Index of final height
        deltaz		Grid spacing in z-direction

        curvature	Derivative of the force in z direction times deltaz in [eV/Å]
				Calculated using Central Difference"""
        forces = self.calculate_forces(zindex, findex, deltaz)
        curvature = (forces[:, :, zindex+1:findex+1] - forces[:, :, zindex-1:findex-1])*0.5
        self.verboseprint("Curvature Min", curvature.min(), "Curvature Max", curvature.max())
        return curvature


class NonContactAFM(NCAFMDFTD3, NCAFMGPAW, NCAFMGPAW5Pt):
    """Wrapper Class for calling a method dependent
    nc-AFM Calculation

    The method argument is used to determine how
    the curvature should be calculated."""

    def get_curvature(self, zindex, findex, deltaz):
        """Generic wraper for the get_curvature method

        zindex		Index of minimum height
        findex		Index of final height
        deltaz		Grid spacing in z-direction

        curvature	Derivative of the force in z direction in [eV/Å²]"""
        if self.kwargs['method'].lower() == 'gpaw':
            return NCAFMGPAW.get_curvature(self, zindex, findex, deltaz)
        elif self.kwargs['method'].lower() == 'gpaw5pt':
            return NCAFMGPAW5Pt.get_curvature(self, zindex, findex, deltaz)
        elif self.kwargs['method'].lower() == 'dftd3':
            return NCAFMDFTD3.get_curvature(self, zindex, findex, deltaz)
        elif self.kwargs['method'].lower() == 'both':
            curvature = NCAFMGPAW.get_curvature(self, zindex, findex, deltaz)
            curvature += NCAFMDFTD3.get_curvature(self, zindex, findex, deltaz)
            return curvature
        else:
            raise NotImplementedError(self.kwargs['method'])


def read_arguments():
    """Input Argument Parsing"""
    parser = ArgumentParser()
    parser.add_argument('filename', type=str,
                        help='name of input GPAW file')
    parser.add_argument('-z', '--zmin',
                        help='minimum height in Å above highest atom (%(default)s 1.0 Å)',
                        default=1., type=float)
    parser.add_argument('-a', '--amplitude',
                        help='amplitude of oscillation in Å (%(default)s 0.6 Å)',
                        default=0.6, type=float)
    parser.add_argument('-r', '--redo',
                        help='redo calculation',
                        action='store_true')
    parser.add_argument('-Rx', '--repeatx',
                        help='times to repeat unit cell in x-direction (%(default)s 1)',
                        type=int, default=1)
    parser.add_argument('-Ry', '--repeaty',
                        help='times to repeat unit cell in y-direction (%(default)s 1)',
                        type=int, default=1)
    parser.add_argument('-g', '--gridrefinement',
                        help='degree of grid refinement (%(default)s 1)',
                        type=int, default=1)
    parser.add_argument('-v', '--verbose',
                        help='increase output verbosity',
                        action='store_true')
    parser.add_argument('-p', '--plot',
                        help='Plot contour',
                        action='store_true')
    parser.add_argument('-pw', '--plotwireframe',
                        help='Plot wire frame schematic',
                        action='store_true')
    parser.add_argument('-m', '--method',
                        help="Method used for calculating nc-AFM."+
                        "One of 'gpaw', 'gpaw5pt', dftd3', or 'both'"+
                        "(%(default)s DFTD3)",
                        default='DFTD3')
    parser.add_argument('-o', '--outfilename',
                        help='output file (%(default)s filename)',
                        type=str, default='')
    parser.add_argument('-dz', '--deltaz',
                        help='grid spacing in Å (%(default)s 0.2 Å)',
                        type=float, default=0.2)
    parser.add_argument('--wmin',
                        help='Minimum frequency (%(default)s 0.0 Hz)',
                        type=float, default=0.0)
    parser.add_argument('--wmax',
                        help='Maximum frequency (%(default)s 0.0 Hz)',
                        type=float, default=0.0)
    parser.add_argument('-wres',
                        help='Resonant frequency (%(default)s 20878.6 Hz)',
                        type=float, default=20878.6)
    parser.add_argument('-k',
                        help='Spring constant (%(default)s 112.35 eV/Å²)',
                        type=float, default=112.35)
    pargs = parser.parse_args()
    if pargs.outfilename == '':
        outfilename = pargs.filename.split('.gpw')[0]
    else:
        outfilename = pargs.outfilename
    outfilename += '_z='+str(pargs.zmin)
    outfilename += '_'+pargs.method+'_ncAFM'
    outfilename += '.png'
    pargs.outfilename = outfilename
    return pargs


def main():
    """Main Executable"""
    # Read Arguments
    args = read_arguments()
    ncafm = NonContactAFM(args.filename,
                          zmin=args.zmin,
                          amplitude=args.amplitude,
                          gridrefinement=args.gridrefinement,
                          repeatx=args.repeatx,
                          repeaty=args.repeaty,
                          redo=args.redo,
                          verbose=args.verbose,
                          plotwireframe=args.plotwireframe,
                          method=args.method,
                          h=args.deltaz)
    ncafm.write_pickle(args.outfilename)
    if args.plot:
        if not args.wmin and not args.wmax:
            ncafm.plot(args.outfilename)
        else:
            ncafm.plot(args.outfilename,
                       wmin=args.wmin,
                       wmax=args.wmax)
        if MPI.COMM_WORLD.rank == 0:
            show()

if __name__ == '__main__':
    main()
