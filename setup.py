from distutils.core import setup

setup(name='non-contact-atomic-force-microscopy',
      version='0.1',
      description='Non-contact atomic force microscopy simulator',
      maintainer='Duncan John Mowbray',
      maintainer_email='duncan.mowbray@gmail.com',
      url='https://gitlab.com/non-contact-atomic-force-microscopy/noncontactafm',
      platforms=['unix'],
      license='GPLv3+',
      scripts=['bin/non-contact-afm'],
      packages=['noncontactafm'])
