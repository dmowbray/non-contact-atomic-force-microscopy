#!/usr/bin/env gpaw-python

"""Test for non-contact atomic force microscopy (nc-AFM)
simulation of benzene"""

from __future__ import print_function
from os.path import exists
from matplotlib.pyplot import show
from ase.build import molecule
from gpaw.test import equal
from gpaw import GPAW, FermiDirac
from noncontactafm.ncafm import NonContactAFM

def main():
    """Benzene nc-AFM test
    LDA, dzp, 1x1x1 k-point sampling"""

    xcf = 'LDA'
    basis = 'dzp'
    kpt = 1
    name = 'benzene_'+xcf+'_'+basis+'_'+str(kpt)+'x'+str(kpt)+'.gpw'
    if not exists(name):
        atoms = molecule('C6H6')
        atoms.center(4)
        atoms.set_pbc(True)
        calc = GPAW(mode='lcao',
                    xc=xcf,
                    h=0.26,
                    basis=basis,
                    kpts=[kpt, kpt, kpt],
                    occupations=FermiDirac(width=0.001))
        atoms.set_calculator(calc)
        atoms.get_potential_energy()
        calc.write(name, mode='all')
    ncafm = NonContactAFM(name,
                          zmin=1.7,
                          #repeatx = 2,
                          #repeaty = 2,
                          verbose=True,
                          method='GPAW',)
    ncafmshape = (48, 48)
    ncafmcenter = 65.1094856463
    ncafmside = 315.281400148
    print("ncafmshape =", ncafm.deltaw.shape)
    print("ncafmcenter =", ncafm.deltaw[24, 24])
    print("ncafmside =", ncafm.deltaw[19, 21])
    equal(ncafm.deltaw.shape[0], ncafmshape[0], 1e-8)
    equal(ncafm.deltaw.shape[1], ncafmshape[1], 1e-8)
    equal(ncafm.deltaw[24, 24], ncafmcenter, 1e-6)
    equal(ncafm.deltaw[19, 21], ncafmside, 1e-6)
    return ncafm

NCAFM = main()
if __name__ == '__main__':
    NCAFM.plot('benzene')
    show()
