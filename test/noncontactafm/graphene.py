#!/usr/bin/env gpaw-python

"""Test for non-contact atomic force microscopy (nc-AFM)
simulation of graphene"""

from __future__ import print_function
from os.path import exists
from matplotlib.pyplot import show
from ase import Atom, Atoms
from ase.units import Bohr
from gpaw.test import equal
from gpaw import GPAW, FermiDirac
from noncontactafm.ncafm import NonContactAFM

def main():
    """Graphene nc-AFM test
    LDA, dzp, 31x31x1 k-point sampling"""

    xcf = 'LDA'
    basis = 'dzp'
    kpt = 31
    name = 'graphene_'+xcf+'_'+basis+'_'+str(kpt)+'x'+str(kpt)+'.gpw'
    if not exists(name):
        # Graphene Cell Parameter in Bohr
        acell = 4.651 * Bohr
        cell = [[acell, 0, 0],
                [-0.5*acell, 3**0.5/2.*acell, 0],
                [0, 0, 8.32]]

        atoms = Atoms()
        atoms.append(Atom('C', [0.5*acell, -acell/(2*3**0.5), 0]))
        atoms.append(Atom('C', [0.5*acell, acell/(2*3**0.5), 0]))
        atoms.set_cell(cell)
        atoms.set_pbc(True)
        calc = GPAW(mode='lcao',
                    xc=xcf,
                    h=0.26,
                    basis=basis,
                    kpts=[kpt, kpt, 1],
                    occupations=FermiDirac(width=0.001))
        atoms.set_calculator(calc)
        atoms.get_potential_energy()
        calc.write(name, mode='all')
    ncafm = NonContactAFM(name,
                          zmin=1.7,
                          #repeatx = 7,
                          #repeaty = 5,
                          verbose=True,
                          method='GPAW',)
    ncafmshape = (8, 8)
    ncafmcenter = 255.873681565
    ncafmside = 63.7593425088
    print("ncafmshape =", ncafm.deltaw.shape)
    print("ncafmcenter =", ncafm.deltaw[0, 4])
    print("ncafmside =", ncafm.deltaw[0, 0])
    equal(ncafm.deltaw.shape[0], ncafmshape[0], 1e-8)
    equal(ncafm.deltaw.shape[1], ncafmshape[1], 1e-8)
    equal(ncafm.deltaw[0, 4], ncafmcenter, 1e-6)
    equal(ncafm.deltaw[0, 0], ncafmside, 1e-6)
    return ncafm

NCAFM = main()
if __name__ == '__main__':
    NCAFM.plot('graphene')
    show()
